#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

int pid;

struct sigaction act_fils, act_pere;

void fonction_fils() {
    printf("Temperature mesure : %d\n", (rand()%30+10));
    fflush(stdout);
}

void fonction_pere() {
    kill(pid, SIGUSR1);
    alarm(5);
}

int main(){
    pid = fork();
    if (pid == 0){
        act_fils.sa_handler = fonction_fils;
        sigaction(SIGUSR1, &act_fils, 0);
        while(1){
            pause();
        }
    } else {
        act_pere.sa_handler = fonction_pere;
        sigaction(SIGALRM, &act_pere, 0);
        alarm(5);
        while(1){
            printf("-");
            fflush(stdout); // vider le buffer
            sleep(1);
        }
    }

}