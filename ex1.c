#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void handler_sigalrm(){
    printf("Temperature mesure : %d\n", (rand()%30+10));
    // fflush(stdout);
}

int main(){
    int pid;
    pid = fork();
    if (pid == 0){
        printf("Processus fils, pid = %d\n", getpid());
        struct sigaction nvt;
        nvt.sa_handler = &handler_sigalrm;
        nvt.sa_flags = 0;
        sigaction(SIGALRM, &nvt, 0);
        while(1){
            pause();
        }
    } else {
        printf("Processus pere, pid = %d\n",getpid());
        while(1){
            for(int i=0; i<5; i++){
                printf("-");
                fflush(stdout);
                sleep(1);
            }
            printf("\n");
            // fflush(stdout);
            kill(pid, SIGALRM);
            // alarm(5); //demande system envoi un SIGALRM apres 5 seconde
        }
        
    }

    return 0;
}