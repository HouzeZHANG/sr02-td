#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h>

extern void initrec();
extern int attendreclic();
extern void rectvert(int n);
extern void detruitrec();
extern void ecritrec(char *buf, int lng);
extern void setbigrec (char *col);

int pid_fils;
int pid_pere;
int sigint_count_pere = 0;
int sigint_count_fils = 0;
bool demande_rectvert = false;

struct sigaction act_fils, act_pere;

void captfils() {
    sigint_count_fils++;
    printf("FILS %d : signal %d recu\n", pid_fils, sigint_count_fils);
    if (sigint_count_fils >= 3){
        detruitrec();
        printf("FILS : fin du fils , trois signals sont deja recu\n");
        exit(0);
    }
    rectvert(5);
    demande_rectvert = true;
}

void captpere() {
    sigint_count_pere++;
    printf("PERE %d : signal %d recu\n", pid_pere, sigint_count_pere);
    if (sigint_count_pere >= 3){
        printf("PERE : fin du pere , trois signals sont deja recu\n");
        exit(0);
    }
}

int main(){
    int n;
    pid_fils = fork();
    if (pid_fils == 0){
        pid_fils = getpid();
        pid_pere = getppid();
        printf("Processus fils, pid_fils = %d, pid_pere = %d\n",pid_fils, pid_pere);

        initrec();
        act_fils.sa_handler = captfils;
        sigaction(SIGINT, &act_fils, 0);
        int i;
        do{
            i = attendreclic();
            switch(i){
                    case 0:
                        kill(pid_pere, SIGINT);
                        break;
                    case 1:
                    case 2:
                    case 3:
                        printf("FILS : pid du mon pere est : %d\n",pid_pere);
                        break;
                    default:
                        break;
            }
        }while(i != -1);
        printf("FILS : fin du fils apres clic sur FIN\n");
        
    } else {
        pid_pere = getpid();
        printf("Processus pere, pid_fils = %d, pid_pere = %d\n",pid_fils, pid_pere);

        act_pere.sa_handler = captpere;
        sigaction(SIGINT, &act_pere, 0);
        
        while (1) {
            n = sleep(10);
            if (n > 0) {
                printf("Père: Temps restant d'attente: %d s\n", n);
            }
        }
    }

}